TARGET = ColorDetection
TEMPLATE = app

CONFIG =  warn_off release

QMAKE_CXXFLAGS += -O3

MOC_DIR = .moc
OBJECTS_DIR = .obj


CONFIG += qt
QMAKE_CXXFLAGS = -std=c++11

INCLUDEPATH += ./sdk/include/
INCLUDEPATH += /usr/local/include
INCLUDEPATH += /usr/local/include/
INCLUDEPATH += ./src


LIBS += ../sdk/libMVSDK.so
LIBS += /usr/local/lib/libopencv_highgui.so.2.4.13
LIBS += /usr/local/lib/libopencv_imgproc.so.2.4.13
LIBS += /usr/local/lib/libopencv_core.so.2.4.13

SOURCES += main.cpp \
    ./window/mainwindow.cpp \
    ./window/capturethread.cpp \
    ./src/detect.cpp

HEADERS += ./window/mainwindow.h \
    ./window/capturethread.h \
    ./window/ui_mainwindow.h \
    ./sdk/include/CameraApi.h \
    ./src/detect.h
FORMS += ./window/mainwindow.ui
